from typing import Tuple
from pyspark import TaskContext
import pandas as pd
from sklearn.model_selection import cross_val_score
import numpy as np
from sksurv.ensemble import RandomSurvivalForest
from sksurv.svm import FastSurvivalSVM
from core import run_experiment
import logging
import time
from pyspark import SparkConf, SparkContext
from typing import Optional
import socket
from datetime import datetime

# Habilito los loggers

logging.getLogger().setLevel(logging.INFO)

# Cantidad de procesadores de la maquina utilizados para el Cross Validation. -1 = usar todos
N_JOBS = -1

# Para aleatoridad replicable
RANDOM_STATE = 20

# If True Random Forest is used as classificator. SVM otherwise
USE_RF: bool = False

# Number of independent complete runs to get the best parameters
NUMBER_OF_INDEPENDENT_RUNS = 3

# To run in an Apache Spark cluster
RUN_IN_SPARK = True

# To use a Broadcast value instead of a pd.DataFrame
USE_BROADCAST = True

# Only if RUN_IN_SPARK is set to True the following parameters are used
# Executors per instance of each worker
EXECUTORS: Optional[str] = "1"

# Cores on each executor
# CORES_PER_EXECUTOR: Optional[str] = "2"
CORES_PER_EXECUTOR: Optional[str] = None

# RAM to use per executor
MEMORY_PER_EXECUTOR: str = "6g"

# Classificator
if USE_RF:
    CLASSIFIER = RandomSurvivalForest(n_estimators=100,
                                      min_samples_split=10,
                                      min_samples_leaf=15,
                                      max_features="sqrt",
                                      n_jobs=-1,
                                      random_state=RANDOM_STATE)
else:
    CLASSIFIER = FastSurvivalSVM(rank_ratio=0.0, max_iter=1000, tol=1e-5, random_state=RANDOM_STATE)


def compute_cross_validation(subset: pd.DataFrame, y: np.ndarray) -> float:
    """
    Computa una validacion cruzada calculando el accuracy
    :param subset: Subset de features a utilizar en el RandomForest evaluado en el CrossValidation
    :param y: Clases
    :return: Promedio del accuracy obtenido en cada fold del CrossValidation
    """
    start = time.time()
    res = cross_val_score(
        CLASSIFIER,
        subset,
        y,
        cv=10,
        n_jobs=N_JOBS
    )
    end_time = time.time() - start
    concordance_index_mean = res.mean()
    logging.info(f'Cross validation with {subset.shape[1]} features -> {end_time} seconds | '
                 f'Concordance Index -> {concordance_index_mean}')

    return concordance_index_mean


def compute_cross_validation_spark(subset: pd.DataFrame, y: np.ndarray) -> Tuple[float, float, int, str, int, str]:
    """
    Computes a cross validations to get the concordance index in a Spark environment
    :param subset: Subset of features to compute the cross validation
    :param y: Y data
    :return: Result tuple with fitness value, execution time, Partition ID, Hostname, number of evaluated features and \
    time-lapse description
    """
    start = time.time()
    res = cross_val_score(
        CLASSIFIER,
        subset,
        y,
        cv=10,
        n_jobs=N_JOBS
    )
    end_time = time.time()
    worker_time = end_time - start
    concordance_index_mean = res.mean()
    logging.info(f'Cross validation with {subset.shape[1]} features -> {worker_time} seconds | '
                 f'Concordance Index -> {concordance_index_mean}')

    partition_id = TaskContext().partitionId()

    # Gets a time lapse description to check if some worker is lazy
    start_desc = datetime.fromtimestamp(start).strftime("%H:%M:%S")
    end_desc = datetime.fromtimestamp(end_time).strftime("%H:%M:%S")
    time_description = f'{start_desc} - {end_desc}'

    return concordance_index_mean, worker_time, partition_id, socket.gethostname(), subset.shape[1], time_description


def main():
    # Configuracion de Spark
    if RUN_IN_SPARK:
        conf = SparkConf().setMaster("spark://master-node:7077").setAppName(f"BBHA {time.time()}")

        if EXECUTORS is not None:
            conf = conf.set("spark.executor.instances", EXECUTORS)

        if CORES_PER_EXECUTOR is not None:
            conf = conf.set("spark.executor.cores", CORES_PER_EXECUTOR)

        if MEMORY_PER_EXECUTOR is not None:
            conf = conf.set("spark.executor.memory", MEMORY_PER_EXECUTOR)

        sc = SparkContext(conf=conf)
        sc.setLogLevel("ERROR")
        fitness_function = compute_cross_validation_spark
    else:
        fitness_function = compute_cross_validation
        sc = None

    model_description = 'RF' if USE_RF else 'SVM'
    run_improved_bbha = False if RUN_IN_SPARK else None  # TODO: improved BBHA it's not implemented for Spark right now

    run_experiment(
        filter_top_50=False,
        run_improved_bbha=run_improved_bbha,
        run_in_spark=RUN_IN_SPARK,
        compute_cross_validation=fitness_function,
        sc=sc,
        metric_description='concordance index',
        add_epsilon=not USE_RF,  # Epsilon is needed by the SVM, not the RF
        model_description=f'{model_description} Survival',
        number_of_independent_runs=NUMBER_OF_INDEPENDENT_RUNS,
        use_broadcasts_in_spark=USE_BROADCAST
    )


if __name__ == '__main__':
    main()
