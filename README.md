# Paper

## Introducción

Lo desarrollado acá buscan implementar los algoritmos definidos en los siguientes papers:
1. [Binary black hole algorithm for feature selection and classification on biological data](https://www.sciencedirect.com/science/article/abs/pii/S1568494617301242?via%3Dihub): la metaheurística en sí misma. 
1. [Improved black hole and multiverse algorithms for discrete sizing optimization of planar structures](https://www.tandfonline.com/doi/full/10.1080/0305215X.2018.1540697): el paper que tunea un poco al algoritmo.
1. [A Parallel Random Forest Algorithm for Big Data in a Spark Cloud Computing Environment](https://arxiv.org/abs/1810.07748): implementación de un Random Forest en Spark. **Útil ya que el Random Forest que viene con Spark parece ser lentísimo.**
1. [A Multi Dynamic Binary Black Hole Algorithm Applied to Set Covering Problem](https://link.springer.com/chapter/10.1007%2F978-981-10-3728-3_6): implementación de una variante del algoritmo en Spark. **IMPORTANTE:** lo que tiene este paper es que distribuye el algoritmo de BH pero no los modelos. Es decir, distribuyen lo menos costoso cuando nosotros buscamos paralelizar la parte de los modelos que arrojan la métrica a optimizar (para más información ver la sección de [Procedimiento a seguir](#procedimiento-a-seguir)). Por ende, este paper no nos sería muy útil, quizás para un cita en la sección del estado del arte.


## Instalación 

Para correr el código hay que instalar las dependencias:

1. Crear un virtual env: `python3 -m venv venv` (una única vez).
1. Activar el virtual env: `source venv/bin/activate` (solo cuando se utilice)
    1. Para salir del virtual env ejecutar: `deactivate`
1. Instalar las dependencias: `pip install -r requirements.txt`


## Datasets

El dataset utilizado es [Breast Invasive Carcinoma (TCGA, PanCancer Atlas)][survival-dataset] (que puede encontrarse listado en [la página de datasets de cBioPortal][cbioportal-datasets]). Los archivos utilizados se encuentran en la carpeta `Datasets`.


## Organización del código

El código principal está en el archivo `core.py` donde se ejecuta la metaheurística y se informan y guardan los resultados en un archivo CSV que irá a parar a la carpeta `Results`. En `main.py` se definen las funciones y parámetros que se pasa al `core` para ejecutar.  De esta manera se pueden correr los experimetos secuenciales (Python plano) y los desarrollados en un entorno Spark de manera limpia ya que ambos enfoques comparten el pre-procesamiento, las corridas independientes y el algoritmo de la metaheurística.

En el archivo `utils.py` se encuentran las funciones de importación, de  binarización de la columna del label del dataset, de preprocesamiento, entre otras funciones útiles.

El algoritmo de la metaheurística y sus variantes se pueden encontrar en el archivo `metaheuristics.py`.

El archivo `times.py` contiene el código para evaluar cuánto tarda la ejecución utilizando diferentes cantidades de features. En `plot_times.py` se encuentra el código para graficar dichos tiempos utilizando el archivo JSON generado con el primer script.

El archivo `memory_leak.py` contiene un benchmark con dos soluciones al memory leak producido en Spark durante el llamado a funciones dentro de un mapPartition. Tanto el problema como la solución están bien explicados en esta pregunta de [Stack Overflow][so-memory-leak].

En el archivo `cross_validation_experiments` se lleva a cabo una serie de experimentos con el CrossValidation y los modelos Survival-SVM. Esto permite entender un poco mejor el funcionamientos de los parámetros. La motivación de este archivo nace de encontrarse con un fitness decreciente tanto para training como para testing a medida que el número de parámetros aumentaba (independientemente del kernel que se utilizaba para el modelo SVM).

Para conocer más sobre los modelos SVM Survival o Random Survival Forest leer el blog de [Scikit-survival][scikit-survival-blog]


## Ejecución de scripts

Spark tiene problemas con la importación de modulos definidos por el usuario, por lo que se deja un archivo llamado `scripts.zip` que contiene todos los modulos necesarios. Ahora solo hay que correr los siguientes comandos para que funcione todo:

1. Configurar todo en el `main.py` con los parámetros a ejecutar.
2. Estando fuera del cluster ejecutar para poder utilizar los modulos de Python: `./zip_modules.sh`
3. Estando dentro del Master del cluster: `spark-submit --py-files scripts.zip main.py`


## Como manejar los resultados

Para organizar todos los resultados se realizan los siguientes pasos:

1. Correr el script direccionando tanto el stderr como el stdout a un archivo. Por ejemplo: `spark-submit --py-files scripts.zip main.py &> logs_my_script.txt`. Cuando termine tanto el script secuencial como el de Spark, quedará en la carpeta `Results` un `.csv` con el datetime exacto en el que se corrió el script con todos los resultados obtenidos.
1. Poner el `logs_my_script.txt` junto con un el `.csv` generado en la carpeta `Results/results_to_push/[datetime]/` (se debe crear manualmente esa carpeta).
    1. Opcional: renombrar el `.txt` para que también tenga el datetime. De esa forma deberían quedar dos archivos `[datetime].csv` y `[datetime].txt` en la carpeta `[datetime]`
1. Crear un `README.txt` que contenga información sobre el experimento, como:
    - Si es secuencial o se lanzó en un cluster Spark.
    - Si se usaron parámetros personalizados o se dejó todo por defecto.
    - Cualquier otro dato que se considere útil para el paper.
1. Pushear ambos archivos!


[scikit-survival-blog]: https://scikit-survival.readthedocs.io/en/stable/user_guide/understanding_predictions.html
[survival-dataset]: https://cbioportal-datahub.s3.amazonaws.com/brca_tcga_pan_can_atlas_2018.tar.gz
[cbioportal-datasets]: https://www.cbioportal.org/datasets
[so-memory-leak]: https://stackoverflow.com/questions/53105508/pyspark-numpy-memory-not-being-released-in-executor-map-partition-function-mem/71700592#71700592