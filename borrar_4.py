# Prueba de distribucion de carga
import numpy as np
from math import ceil


def assign_ids(stars_subsets: np.ndarray, number_of_workers: int):
    """
    Assigns partitions IDs equally among all the stars
    :param stars_subsets: Stars numpy array. First element is index, second is subset of features
    :param number_of_workers: Number of workers to compute the partitions equally
    :return:
    """
    current_n_stars = len(stars_subsets)
    rows_per_partition = ceil(current_n_stars / number_of_workers)
    partition_id = 0
    current_total_rows_per_part = 0
    last_row = number_of_workers - 1
    for i in range(current_n_stars):
        stars_subsets[i][0] = partition_id
        current_total_rows_per_part += 1
        if current_total_rows_per_part == rows_per_partition and partition_id != last_row:
            partition_id += 1
            current_total_rows_per_part = 0


def main():
    n_stars = 15
    total_n_features = 20_000
    stars_subsets = np.empty((n_stars, 2), dtype=object)  # 2 = (1, features)
    step = 100
    current_n_features = step
    number_of_workers = 3
    while current_n_features <= total_n_features:
        for i in range(n_stars):
            random_features_to_select = np.zeros(total_n_features, dtype=int)
            random_features_to_select[:current_n_features] = 1
            np.random.shuffle(random_features_to_select)
            stars_subsets[i] = (i, random_features_to_select)

            current_n_features += step

            # If it's arraised the maximum number of features, slices the stars array
            if current_n_features > total_n_features:
                stars_subsets = stars_subsets[:i + 1]
                break

        # Assigns partition ID to idx
        assign_ids(stars_subsets, number_of_workers)

        # Esto seria para generar las particiones por cada lote de 15 estrellas
        # res = list(split(stars_subsets, n=3))
        print(stars_subsets)


main()
